\documentclass[a4paper, oneside]{discothesis}

% use utf8 instead of latin1 when using LaTeX in windows
\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{bm}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DOCUMENT METADATA

\thesistype{Bachelor Thesis}
\title{Enter your thesis title here}

\author{Matthias Lei}
\email{mlei@student.ethz.ch}
\institute{Distributed Computing Group \\[2pt]
Computer Engineering and Networks Laboratory \\[2pt]
ETH Z\"urich}

% You can put in your own logo here "\includegraphics{...}" or just comment the command
% \logo{}

\supervisors{Christian Decker\\[2pt] Prof.\ Dr.\ Roger Wattenhofer}

% You can comment the following two commands if you don't need them
% \keywords{Keywords go here.}
% \categories{ACM categories go here.}

\date{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\frontmatter % do not remove this line
\maketitle

\cleardoublepage

\begin{acknowledgements}
I would like to thank my supervisor Christian Decker for his assistance and guidance during my bachelor thesis as well as Ingrid Giel for proofreading and reviewing my report.
\end{acknowledgements}


\begin{abstract}
As a decentralized digital currency, one of the features Bitcoin draws its security from is that its clients broadcast transaction or block messages to a number of randomly selected peers. This feature makes it very hard or impossible for attacks which rely on knowing the peers of a victim. One of these attacks is the so called double-spend. And over the course of this thesis we will examine the influence on the chance of success given that one has identified the peers.

The thesis is split into two parts.  First we introduce [fancy name], a program which reveals the peers of a Bitcoin Core client with very high probability within a few minutes based on timestamps. In the second part we evaluate the success rate of double-spends once the attacker identified all the peers of the victim using [fancy name].
    
\end{abstract}

\tableofcontents

\mainmatter % do not remove this line

% Start writing here
\chapter{Introduction}

Bitcoin made its first appearance in 2009 and since then it steadily gained popularity which found its end in the bankruptcy of Mt.Gox, one of the largest exchange platforms for Bitcoin. But the digital currency still remains being used and altough it is not present in the media anymore Bitcoin has rather found its place in the economy as an interesting alternative to regular payment methods and in computer science as an important step towards a well established decentralised digital currency system.

The usage of Bitcoin can be found all over the globe. Online as well as regular shops accept Bitcoin as a payment method. There are still, apart from Mt.Gox, a number of large exchange platforms for Bitcoin such as Coinbase or Bitstamp. And furthermore Bitcoin ATMs have been deployed in many countries~\cite{ATM}.
Many of these usages rely on fast payment, meaning the service offered must be delivered seconds after the transaction has been made instead of waiting the transaction first being confirmed, which takes up several minutes. This scenario is prone to the double-spend attack in which an attacker can use the same coins for two or more payments. However, because the success of double spends is probabilistic and furthermore rather unlikely our hypothesis is that an adversary can boost his success probability by having insight about which peer the victim, who offers the fast payment service, is connected to and then feeding those peers specific information about transactions made.

There are many different clients which implement the Bitcoin protocol and enable users to store and trade with Bitcoins. For the course of this thesis we will only look at the most common one: Bitcoin Core. We will use this client as an example of the protocol, but similar results can be achieved with other clients as we do not exploit specific properties of this client instead we will only rely on the Bitcoin protocol. Bitcoin Core is also sometimes referred as the Satoshi client in honor of the creator of Bitcoin Satoshi Nakamoto~\cite{white_paper}. For the following chapters we will use the terms Bitcoin Core and Satoshi client interchangeably.

\section{Transaction}

Bitcoins are transferred from one Bitcoin address to another through transactions. Typically, a digital wallet stores several of these addresses. Each transaction consists of at least one input and one output and the number of Bitcoins transferred to each output (for the scope of this thesis we do not look at coinbase transactions), where the inputs represent the origins of a transaction and the outputs the destinations. The inputs point to outputs of previous transactions creating a chain which is public and where everyone is able to track and verify the complete transaction history since the beginning of bitcoin. However anonymity is still (almost[]) guaranteed as these addresses are merely virtual pseudonyms. In order to claim an output (and use it as an input) one has to prove the ownership of that output, and therefore the ownership of these coins bound to it, by using the private key associated with the address.
When making a payment a transaction with all its necessary parts is created and signed before broadcast into the network. Every node receiving the transaction then validates it and faulty or incomplete transactions are simply not going to be relayed. In particular the node checks whether the output claimed has not been used before. Prior to every transaction an \textit{inv} message is sent containing the hash of the transaction. The receiving node can check whether it has already seen the transaction and if not it requests the full transaction with a \textit{getdata} message. The same behaviour applies for block messages.

The Satoshi client will only relay the first transaction, but any subsequent transaction claiming the same output will not be relayed. However there are patches and builds which do not follow this behaviour, e.g. BitcoinXT or an alternative build of the Satoshi client, in order to signalise to their neighbours that someone attempts a double-spend. This is one of the main reasons why no higher success rate could be achieved in the evaluation in chapter 3.

\section{Blockchain}

The blockchain is Bitcoin's famous public ledger. It is Bitcoin's approach of (eventually) reaching consensus in its peer-to-peer network about, which transactions have effectively happened and in which order. The blockchain consists, in the best case, of a single path of blocks, starting from the first genesis block all the way to the current block. Every block carries a timestamp, a hash to the most previous block at the time mined, a collection of transactions and a nonce. So following the path from the first to the last block is equivalent to reviewing all the transactions ever made in chronological order. therefore a transaction contained in a block must have its inputs point to transactions in previous blocks.
Creating one of these blocks is based on a proof-of-work (PoW) system and requires a significant amount of computational power. By appending the right nonce to the binary representation of the payload mentioned above, the value of a given pseudorandom hashfunction must lie below a target value in order to be accepted by the network. This target value is adjusted dynamically, so that in average such a block is found roughly every ten minutes. Some nodes, called miners, in the network are solely dedicated to finding such blocks. They listen to newly announced transactions in the network, validate them and keep them in a pool. For every block found the miner is rewarded with a certain amount of Bitcoins plus the sum of all transaction fees from the transactions included in the block. As soon as a block is found the miner broadcasts it into the network and the receiving nodes can check for validity by computing the hash of the block and verifying all the transaction contained and, if valid, append it to his own blockchain. It is important to point out that modifying the block (and thus claiming the reward for its own sake) is equally as hard as mining a new block because every modification to a block results in a change of the hash value.

It can happen that during the propagation of a newly mined block another block is found by another miner, which has not yet heard of the new block, leading to a split of the network and ultimately to two different views of the transaction history ~\cite{information_propagation}. This is called a fork. 
Resolving such both partitions enter a race about which one finds the next block and creating a longer chain, which has to be at the end accepted by both parties. The race can potentially go on for multiple blocks but the probability of it decreases polynomial for every additional block. However if a miner happens to create a longer chain originating from a non-current block, and thus creating another history the rest of the network has to rollback its history to the point where the fork happened. But this requires a majority of the mining power of the Bitcoin network ~\cite{51percent}.

\begin{figure}[ht]
	\centering
    \includegraphics[scale=0.5]{blockchain.eps}
	\caption{Example of a blockchain segment}
\end{figure}

\section{Address Propagation}

Every node manages a pool of IP addresses which it can resort to if a connection to another Bitcoin peer is lost. those peers are used to broadcast and relay data messages, such as addresses, blocks or transactions. 
by default the Satoshi client runs 8 outgoing connections. on first Startup of the client it performs a DNS lookup in order to obtain a list of ip addresses believed to be active. the URLs of the DNS servers are hardcoded into the Satoshi client. It then connects itself to those peers and starts exchanging information about their view of the network or it broadcasts a random selection addresses from its pool, unsolicited or on request with a \textit{getaddr} message. Every address in a \textit{addr} packet carries a timestamp of the most recent appearance in the network. This will be crucial for our program to find out to which peers a client is connected to. Before version 0.10.0 of the Satoshi client an \textit{addr} packet contained almost always 1000 addresses, which are selected randomly. Since the release of version 0.10.1 an textit{addr} packet only contains roughly 300 addresses due to a change in policy for selecting the addresses. Further details about the change are examined in chapter 2.3.

\section{Double-spend}

The double-spend attack uses the fact that, when multiple transactions claim the same output of a previous transaction, only one of those can enter the blockchain. This can be abused against services who cannot afford to wait until a transaction is confirmed by the blockchain (i.e included in the blockchain) and the service has to be delivered at most after a few seconds, e.g ATMs, cafes or fast food chains.
Given an attacker knows the peers of the victim who runs a Satoshi client, the attacker can feed the peers the transaction which certifies that the service claimed has been paid for. At the same time (or with a slight delay, as we are going to inspect in this thesis) the attacker broadcasts to a much larger number of other peers a different transaction claiming the same output, but pointing to another destination address in the hope that this transaction will be confirmed at the end. The second transaction can be a payment for another service or can be a different address owned by the attacker.
The victim has to assume, upon receiving the transaction dedicated to him, that this transaction is going to be confirmed by the network as it cannot wait for it. This is called a 0/unconfirmed payment. Waiting for the transaction to be confirmed takes about roughly 10 minutes but studies show that duration can deviate by 15 minutes~\cite{information_propagation}. Upon a succesful double-spend the victim will realize that the payment he has been waiting for was rendered invalid as another transaction has already claimed the coin.

\chapter{Revealing peers}

Every node manages a pool of addresses with their respective timestamp about the last point when the address was heard of. Generally speaking\footnote{This describes the rough behaviour. For full details please refer to the Bitcoin Core implementation on github.com/bitcoin/bitcoin}, those addresses are updated under two conditions:
\begin{enumerate}
\item The address was sent as a part of an \textit{addr} message and carries a more recent timestamp than the timestamp stored in the pool.
\item When a connected peer sends a \textit{addr}, \textit{version}, \textit{inv}, \textit{getdata} or a \textit{ping} message the timestamp of his address is then updated in the pool of the receiving node. Note that this behaviour applies to the Satoshi client version 0.10.0 and older, condition 2 has been changed in version 0.10.1 which was released during the writing of this paper and we will address that in chapter 2.3. The measurements for the program were taken before the release of version 0.10.0 and do not apply for the current client anymore.
\end{enumerate}
The key idea of the implementation is that the addresses which the victim is connected to are updated much more frequently because of condition 2. Therefore repeatedly requesting addresses through \textit{getaddr} messages first leads to a relatively good coverage of the victim's address pool and second the potential attacker can sort those addresses which occur in multiple \textit{addr} packets by frequency of timestamp updates.
For the course of this thesis we introduce two terms:
\begin{itemize}
\item Those peers which the victim is connected to are called \textbf{true peers}. By default the Satoshi client runs 8 of these outgoing connections, meaning the victim actively establishes the connection to those peers, in contrast to incoming connections which do not fall under the definition of true peers and are passively accepted by the victim. 
\item Peers which the attacker believes to be potential true peers are called \textbf{local peers}. The set of local peers should be larger than the set of true peers in order to mitigate the chance of having false negatives.
\end{itemize}

\section{Implementation}

For producing an estimate of the true peers [fancy name] tries to take snapshots of the victim's address pool at different points in time. The assumption is that each of those the true peer's addresses have a more recent timestamp due to be updated more frequently. Together with other snapshots a score can be constructed about the addresses which occurred the most often in the x most recent timestamps per snapshot. However there is no direct way to take the snapshots from the victim's address pool. That is why [fancy name] approximates it by sending multiple \textit{getaddr} requests in a short time for having a meaningful coverage of the pool. The whole procedure should not take up too much time as first the peers of the Satoshi client changes from time to time and thus disturb the measurements and second for the attacker's convenience, he wants to have solid results as quickly as possible.
The results in figure 2.1, 2.2 and 2.3 were produced by defining a snapshot as 10 successive \textit{getaddr} requests sent every 2 seconds. Between every snapshot [fancy name] waits 10 seconds until a total of 5 snapshots are taken. 
Within each snapshot all the addresses are then sorted by its timestamp and only the 10 most recent addresses are considered for further evaluation. In a final step every bundle of 10 addresses per snapshot are aggregated into buckets which counts the occurrences over all bundles. The parameters chosen proved to be producing the best results while keeping the running time fairly short.

\section{Setup}

We chose Bitcoin Core as the victim for [fancy name] as this is the most prevalent client in the network. We ran Bitcoin Core on the localhost on default settings, i.e it randomly establishes 8 outgoing connections and monitored them using netstat. After running [fancy name] we checked how good the set of local peers covered the set of true peers. In order to find the best size for the set of local peers, we first chose this set to be very large, so a 100\% coverage was achieved every time, and then identify the smallest possible size, where 100\% coverage could be reached in almost every test. This size could then be used in a real-world scenario. For the double spend attack to succeed it is important to choose the smallest smallest possible set of local peers to prevent the wrong transaction from being confirmed by the network. But if the attacker wishes to have an estimate of the true peers for other purposes where it is not (so) critical to keep the set small, he could do so as well with [fancy name]. The tests were run under three different versions of Bitcoin Core which were released during writing, namely 0.9.3, 0.10.0 and 0.10.1. All three versions brought changes to the peer-to-peer behaviour and these become apparent in the evaluation.

\section{Evaluation}

\subsection{Bitcoin Core v0.9.3}

Figure 2.1 illustrates the results under version 0.9.3 where the number of local hosts could be set to 16 in order to achieve a full coverage of the true peers almost every time. We will use this size as base for chapter 3.

\begin{figure}[ht]
	\centering
    \includegraphics[width=1\textwidth]{peer_coverage_v9_3.png}
	\caption{Coverage under version 0.9.3 with 8 runs}
\end{figure}

\subsection{Bitcoin Core v0.10.0}

Interestingly when version 0.10.0 was released [fancy name] could massively benefit from in the form of having even a smaller set of local peers. Clients running this version would not propagate obviously poor addresses~\cite{change_log_v10_0}, i.e. addresses having an old timestamp or addresses which have had 10 successive failures in a week, etc. For comparison an \textit{addr} response under 0.9.3 usually contained 1000 addresses, whereas under version 0.10.0 an \textit{addr} packet contains only around 300 addresses. As a consequence the probability of including false positives is reduced by a large amount and the size of local peers could be reduced from 16 to 10 as shown in figure 2.2.

\begin{figure}[ht]
	\centering
    \includegraphics[width=1\textwidth]{peer_coverage_v10_0.png}
	\caption{Coverage under version 0.10.0 with 8 runs}
\end{figure}

\subsection{Bitcoin Core v0.10.1}

Version 0.10.1 introduced a change which renders [fancy name] useless. This change dropped update condition 2, i.e. as long as a connection between two peers is maintained none of them updates the respective timestamp when messages are exchanged. On disconnect only the update happens leaving no hints about the update frequency. Figure 2.3 illustrates the effect: no clear estimate of the numbers of local peers is possible~\cite{changelog_v10_1}.

\begin{figure}[ht]
	\centering
    \includegraphics[width=1\textwidth]{peer_coverage_v10_1.png}
	\caption{Coverage under version 0.10.1 with 8 runs}
\end{figure}
\chapter{Double-spend with revealed peers}

Referring to the chapter 1.4 about how a double spend works we introduce three new terms:
\begin{enumerate}
\item \textbf{Other peers} are those ones who are receiving the transaction dedicated to the blockchain. The larger this set is the more likely it is that mining pools receive the right transaction.
\item $\bm{Tx_{v}}$ denotes the transaction which seemingly confirms the payment for the service claimed. Consequently this is the only transaction the victim is supposed to see in this context and he receives it by his peers who in return are fed directly by the attacker. Assuming the peers follow the standard behaviour any subsequent transaction claiming the same output will not be relayed further to the victim and therefore triggering an alarm.
\item $\bm{Tx_{a}}$ denotes the transaction which the attacker broadcasts to the other peers.
\end{enumerate}

For a double spend to be successful two requirements have to be met. First the victim should not hear from $Tx_{a}$ until he has delivered the service. But eventually he is going to detect the double-spend, namely when his blockchain is updated. The case, where the victim hears from $Tx_{a}$ after delivery by a peer who relays the double-spend is extremely unlikely as the transaction propagation time is very low and within the acceptable range of waiting time for a fast payment. The 50th percentile of a transaction to be propagated through the Bitcoin network lies currently around 1 second~\cite{bitcoinstats}. Second $Tx_{a}$ has to be confirmed by the blockchain. this is achieved by announcing $Tx_{a}$ to a large set of other peers.
Sending $Tx_{a}$ and $Tx_{v}$ at the same time turned out to be an issue because of the network latency. In some cases $Tx_{a}$ would still reach the victim faster in at least two hops than $Tx_{v}$ in only one hop. Therefore it is necessary to implement a delay between sending out first $Tx_{v}$ in the hope to sufficiently isolate the victim from the dissemination of $Tx_{a}$. But choosing the delay too large results in $Tx_{a}$ being included. 



\begin{figure}[ht]
	\centering
    \includegraphics[scale=0.7]{scenario.eps}
	\caption{Attack scenario}
\end{figure}

\section{Setup}

For this scenario we mimiced on the victim's side the behaviour of its Bitcoin Core node on default settings, including establishing 8 outgoing connections and requesting the hashes announced in \textit{inv} messages. All the messages exchanged between the victim and his peers are logged into a file for further evaluation. On the attacker's side we assumed he has already ran [fancy name] from chapter 2 with a coverage of 100\% having 16 local peers, i.e. 8 false positives and 8 true positives. Furthermore, the attacker connects himself with 128 other peers which are chosen uniformly at random from getaddr.bitnodes.io/api/v1/snapshots.


The double-spend scenario was set up as follows:
\begin{enumerate}
\item Attacker and Victim connect to the their respective peers.
\item Attacker sends $Inv_{v}$ to the local peers and $Inv_{a}$ to the other peers simultaneously in order to prime both groups.
\item Attacker sends $Tx_{v}$ to the local peers and after waiting a delay of $D_{Tx}$ he sends $Tx_{a}$ to the other peers.
\item Victim listens for messages (and replies if necessary) for another 60 seconds before the test run is terminated.
\end{enumerate}
After waiting for at least an hour we checked which transaction has been confirmed by the blockchain by querying Bitcoin Core and examined the log files to see whether the victim has heard of $Tx_{a}$.
We ran this setup with several different delays ranging from 0.0 seconds to 0.5 seconds with 0.025 as step-size.
In order to measure the improvement we also set up a base scenario as reference. In the base scenario the attacker we assume that he has not run [fancy name] and broadcasts $Tx_{v}$ to 16 randomly chosen peers, but every other parameter remains the same. 

\section{Evaluation}

As expected the base scenario rarely succeeds. The reason for failing solely lies in the early detection of $Tx_{a}$ at the victim's side as both transactions are broadcasted to a random set of peers. While $Tx_{a}$ has a much larger number of recipients than $Tx_{v}$ it is clear that the victim has a very high probability of hearing from the wrong transaction even though $Tx_{v}$ is given a headstart. On the contrary the probality which transaction is ultimately going to be confirmed does not depend on the different choice of the peers. Figure TODO and figure TODO confirm this statement.

\begin{figure}[ht]
	\centering
    \includegraphics[scale=1]{double_spend_success_base.pdf}
	\caption{Success of double-spends with random local peers}

	\centering
    \includegraphics[scale=1]{victim_heard_tx_v_only_base.pdf}
	\caption{Percentage of cases where victim only heard $Tx_{v}$ with random local peers}

	\centering
    \includegraphics[scale=1]{tx_a_in_blockchain_overall_base.pdf}
	\caption{Percentage of cases where blockchain included $Tx_{a}$ with random local peers}
\end{figure}

\chapter{Conclusion}

We showed that it was indeed possible to gain significant advantage for double-spends if the peers are known beforehand. Furthermore we developed a program which is able to find the peers very reliably using timestamps in \textit{addr} packages.
However Bitcoin's attempt in changing the update behaviour of timestamps under version 0.10.1 proved to be succesful as it leaves no fingerprints of the currently connected peers on a client anymore. But if an attacker manages to identify the true peers on an alternative way (which can involve methods that operate beyond the bitcoin protocol) then he is able to achieve the performance showed in chapter 3.2.

Bitcoin still offers so far no sufficient protection mechanism against double-spend attacks. 
Some attempts have been made to improve the situation. One mechanism clearly observed from the results was the policy of relaying double-spends. This serves as an alert signal to all his neighbours, some of which are potential fast payment merchants. However this requires the merchant to have some mechanism of detecting a double-spend or at least, if the merchant is a person, being aware of this threat and watch for his Bitcoin Core client whether an conflicting transaction appears. It is sufficient that nodes which adhere this relaying policy only need to relay the first double-spend seen to his neighbours in order to avoid congestion if the attacker floods the network with many different double-spend transaction. Bitcoin XT acts probably as the most known example of using this policy~\cite{bitcoinxt}. Bitcoin XT is a patch on top of Bitcoin Core and implements other changes and improvements for the peer-to-peer aspect of Bitcoin.
For fast payments Bitcoin guarantees no absolute security against double-spends and we argue that better detection mechanism emerge as a necessity as it would greatly bring back the trust to merchants who were intimidated by the bankruptcy of Mt.Gox.


% This displays the bibliography for all cited external documents. All references have to be defined in the file references.bib and can then be cited from within this document.
\bibliographystyle{splncs}
\bibliography{references}

% This creates an appendix chapter, comment if not needed.
\appendix
\chapter{Appendix Chapter}

\end{document}
